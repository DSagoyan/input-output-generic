﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Input_Output_2.Models
{
    class Cars
    {
        public int Price { get; set; }
        public string Model { get; set; }
        public Marks Marks { get; set; }
        public Colors Colors { get; set; }

        public string Information => $"{Model} {Marks} {Colors}";

        public override string ToString() => Information;
 
    }
}
