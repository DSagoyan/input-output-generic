﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Input_Output_2
{
    public enum Marks :byte
    {
        BMW,
        Mercedes,
        Audi,
        Ford,
        Honda
    }

    public enum Colors : byte
    {
        Black,
        Silver, 
        Blue,
        White
    }
}
