﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Input_Output_2.Repository
{
    abstract class BaseRepository<T> where T: class, new()
    {
        public abstract string path { get; }

        public abstract void print(StreamWriter writer, T model);

        List<T> list;
        public BaseRepository()
        {
            list = AsEnumerable().ToList();
        }

        public void Add (T Model)
        {
            list.Add(Model);
        }

        public void AddRange (IEnumerable<T> models)
        {
            foreach (var item in models)
            {
                list.Add(item);
            }
        }

        public void Insert (int index, T model)
        {
            list.Insert(index, model);
        }

        public IEnumerable<T> AsEnumerable()
        {
            StreamReader reader = new StreamReader(path, Encoding.Default);
            T model = null;

            string line = reader.ReadLine();
            while (!reader.EndOfStream)
            {
                switch (line)
                {
                    case "{":
                        model = new T();
                        break;
                    case "}":
                        yield return model;
                        break;
                    default:
                        ReadModel(line, model);
                        break;
                }
            }
        }

        public int SaveChanges()
        {
            int count = 0;
            StreamWriter writer = new StreamWriter(path);
            foreach (var item in list)
            {
                writer.WriteLine("{");
                print(writer, item);

                writer.WriteLine("}");
                count++;
            }
            writer.Dispose();
            list.Clear();
            return count;
        }

        public abstract void WriteModel(StreamWriter writer, T model);

        public abstract void OnReadModel(string[] data, T model);

        public void ReadModel(string line, T model)
        {
            string[] data = line.Split(':');

            if (data.Length < 2)
            {
                return;
            }
            OnReadModel(data, model);
        }