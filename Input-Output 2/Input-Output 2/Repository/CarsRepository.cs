﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Input_Output_2.Models;

namespace Input_Output_2.Repository
{
    class CarsRepository : BaseRepository<Cars>
    {
        public override string path => "Cars.txt";

        public override void OnReadModel(string[] data, Cars model)
        {
            switch (data[0])
            {
                case nameof(model.Marks):
                    if (byte.TryParse(data[1], out byte marks))
                        model.Marks = (Marks)marks;
                    break;
                case nameof(model.Model):
                    model.Model = data[1];
                    break;
                case nameof(model.Colors):
                    if (byte.TryParse(data[1], out byte colors))
                        model.Colors = (Colors)colors;
                    break;
                case nameof(model.Price):
                    if (int.TryParse(data[1], out int price))
                    model.Price = (int)price;
                    break;
            }
               public override void WriteModel(StreamWriter writer, Cars model)
        {
            writer.WriteLine($"{nameof(model.Marks)}:{model.Marks}");
            writer.WriteLine($"{nameof(model.Model)}:{model.Model}");
            writer.WriteLine($"{nameof(model.Colors)}:{model.Colors}");
            writer.WriteLine($"{nameof(model.Price)}:{model.Price}");
        }
    }
    }
}
